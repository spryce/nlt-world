// Set billboards
var Cesium      = require('cesium/Cesium');
const nltlogo   = require("../images/nlt-logo.png");
const valelogo   = require("../images/logo_vale.gif");
const mmglogo   = require("../images/mmg-logo.png");
const enshamlogo   = require("../images/ensham-logo.jpg");
const anglologo   = require("../images/anglo-logo.png");
const riologo   = require("../images/rio-logo.png");


module.exports = (viewer, cameraViews) => {

    // Logos
    viewer.entities.add({        
        name: 'NLT Australia',
        description: nltaDesc,
        position : cameraViews.nlta.location,
        billboard :{
            image : nltlogo,
            heightReference : Cesium.HeightReference.CLAMP_TO_GROUND,
            horizontalOrigin: Cesium.HorizontalOrigin.LEFT,
            verticalOrigin: Cesium.VerticalOrigin.BOTTOM,
            width: 60,
            height: 24
        }
    });
    viewer.entities.add({
        name: 'NLT Global – Northern Light Technologies',
        description: nltcDesc,
        position : cameraViews.nltc.location,
        billboard :{
            image : nltlogo,
            heightReference : Cesium.HeightReference.CLAMP_TO_GROUND,
            horizontalOrigin: Cesium.HorizontalOrigin.LEFT,
            verticalOrigin: Cesium.VerticalOrigin.BOTTOM,
            width: 60,
            height: 24
        }
    });
    viewer.entities.add({    
        name: 'NLT Chile',
        description: nltchDesc,
        position : cameraViews.nltch.location,
        billboard :{
            image : nltlogo,
            heightReference : Cesium.HeightReference.CLAMP_TO_GROUND,
            horizontalOrigin: Cesium.HorizontalOrigin.LEFT,
            verticalOrigin: Cesium.VerticalOrigin.BOTTOM,
            width: 60,
            height: 24
        }
    });


    // Pins
    let pinBuilder = new Cesium.PinBuilder();
    viewer.entities.add({
        name: 'NLT Australia',
        description: nltaDesc,
        position: cameraViews.nlta.location,
        billboard: {
            image: pinBuilder.fromColor(Cesium.Color.ORANGE, 25),
            verticalOrigin: Cesium.VerticalOrigin.TOP,
            heightReference : Cesium.HeightReference.CLAMP_TO_GROUND,
            translucencyByDistance: new Cesium.NearFarScalar(1.5e2, 1.0, 8.0e6, 0.0)
        }        
    });
    viewer.entities.add({
        name: 'NLT Global – Northern Light Technologies',
        description: nltcDesc,
        position: cameraViews.nltc.location,
        billboard: {
            image: pinBuilder.fromColor(Cesium.Color.ORANGE, 25),
            verticalOrigin: Cesium.VerticalOrigin.TOP,
            heightReference : Cesium.HeightReference.CLAMP_TO_GROUND,
            translucencyByDistance: new Cesium.NearFarScalar(1.5e2, 1.0, 8.0e6, 0.0)
        }        
    });
    viewer.entities.add({
        name: 'NLT Chile',
        description: nltchDesc,
        position: cameraViews.nltch.location,
        billboard: {
            image: pinBuilder.fromColor(Cesium.Color.ORANGE, 25),
            verticalOrigin: Cesium.VerticalOrigin.TOP,
            heightReference : Cesium.HeightReference.CLAMP_TO_GROUND,
            translucencyByDistance: new Cesium.NearFarScalar(1.5e2, 1.0, 8.0e6, 0.0)
        }        
    });

    // Customers
    viewer.entities.add({
        name: 'Carborough Downs Mine - Vale',
        description: "",
        position : cameraViews.customer.carb.location,
        billboard :{
            image : valelogo,
            heightReference : Cesium.HeightReference.CLAMP_TO_GROUND,
            horizontalOrigin: Cesium.HorizontalOrigin.LEFT,
            verticalOrigin: Cesium.VerticalOrigin.BOTTOM,
            translucencyByDistance: new Cesium.NearFarScalar(1.5e2, 1.0, 8.0e6, 0.0),
            scaleByDistance: new Cesium.NearFarScalar(1.5e2, 1.0, 8.0e6, 0.0),
            // width: 66,
            // height: 26  
        },           
    });
    viewer.entities.add({
        name: 'Dugald River Mine - MMG Limited',
        description: "",
        position : cameraViews.customer.dugald.location,
        billboard :{
            image : mmglogo,
            heightReference : Cesium.HeightReference.CLAMP_TO_GROUND,
            horizontalOrigin: Cesium.HorizontalOrigin.LEFT,
            verticalOrigin: Cesium.VerticalOrigin.BOTTOM,
            translucencyByDistance: new Cesium.NearFarScalar(1.5e2, 1.0, 8.0e6, 0.0),
            scaleByDistance: new Cesium.NearFarScalar(1.5e2, 1.0, 8.0e6, 0.0),
            // width: 60,
            // height: 28  
        },           
    });
    viewer.entities.add({
        name: 'Ensham Resources',
        description: "",
        position : cameraViews.customer.ensham.location,
        billboard :{
            image : enshamlogo,
            heightReference : Cesium.HeightReference.CLAMP_TO_GROUND,
            horizontalOrigin: Cesium.HorizontalOrigin.LEFT,
            verticalOrigin: Cesium.VerticalOrigin.BOTTOM,
            translucencyByDistance: new Cesium.NearFarScalar(1.5e2, 1.0, 8.0e6, 0.0),
            scaleByDistance: new Cesium.NearFarScalar(1.5e2, 1.0, 8.0e6, 0.0)
        },           
    });
    viewer.entities.add({
        name: 'Grasstree Mine - AngloAmerican',
        description: "",
        position : cameraViews.customer.grass.location,
        billboard :{
            image : anglologo,
            heightReference : Cesium.HeightReference.CLAMP_TO_GROUND,
            horizontalOrigin: Cesium.HorizontalOrigin.LEFT,
            verticalOrigin: Cesium.VerticalOrigin.BOTTOM,
            translucencyByDistance: new Cesium.NearFarScalar(1.5e2, 1.0, 8.0e6, 0.0),
            scaleByDistance: new Cesium.NearFarScalar(1.5e2, 1.0, 8.0e6, 0.0)
        },           
    });
    viewer.entities.add({
        name: 'Kestrel Mine - Rio Tinto',
        description: "",
        position : cameraViews.customer.kestrel.location,
        billboard :{
            image : riologo,
            heightReference : Cesium.HeightReference.CLAMP_TO_GROUND,
            horizontalOrigin: Cesium.HorizontalOrigin.LEFT,
            verticalOrigin: Cesium.VerticalOrigin.BOTTOM,
            translucencyByDistance: new Cesium.NearFarScalar(1.5e2, 1.0, 8.0e6, 0.0),
            scaleByDistance: new Cesium.NearFarScalar(1.5e2, 1.0, 8.0e6, 0.0)
        },           
    });
}

const nltaDesc = `
    22 Cessna Dr. Caboolture, QLD 4510 <br />
    +61 7 5495 2944 <br />
    <a href="http://www.nltinc.com/australia/" target="_blank">http://www.nltinc.com/australia/</a> <br /><br />
    NLT Australia Pty Ltd was formed in Jan 2005 by Managing Director, Tim Haight. The company has grown from a sales and support operation based in Queensland to its current engineering, manufacturing and service operation. NLT Australia now consists of a manufacturing facility in Brisbane with staff of approximately 20 people.        
    `;  

const nltcDesc = `
    33 Laird Drive Toronto, Ontario Canada M4G 3S9<br />
    (416) 425-6559 <br />
    <a href="http://www.nltinc.com" target="_blank">http://www.nltinc.com</a> <br /><br />
    Since 1984, NLT's mission has been to continually develop and deploy the most advanced technologies to make mines safer and more efficient.

    Known globally for our award-winning cap lamp technology, we have directed the same passion, commitment and understanding into the design of our underground communication solutions.
    `;  

const nltchDesc = `
    Espronceda 539 Ñuñoa, Santiago, Chile<br />
    +56223430506 <br />
    <a href="http://nltinc.cl/" target="_blank">http://nltinc.cl/</a> <br /><br />
    Somos una empresa de capital Canadiense para el desarrollo y comercialización de tecnología para la industria minera. Nuestra empresa pertenece a la compañía NLT Northern Light Technologies Canadá (www.nltinc.com).

    NLT Desde su fundación en Toronto Canadá en 1984, Northern Light Technologies (NLT) con su departamento de ingeniería desarrolla y produce sistemas de comunicación de avanzada y lámparas mineras profesionales usando tecnología Led.

    Desde 1997 NLT Chile comercializa diferentes marcas internacionales que dan valor a nuestro portafolio de negocios en Chile.

    Contamos con presencia en los principales países mineros centrando nuestro negocio en Australia, Canadá y Chile.
    `;  



