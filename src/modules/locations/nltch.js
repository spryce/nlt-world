/** NLT Chile views */
module.exports = (viewer) => { 
    const Cesium = require("cesium/Cesium");
    const helpers = require("../camera-helpers")(viewer);

    let flightTime = 10;
    let pitchAdjustHeight = 5000000;

    let nltchLocation = new Cesium.Cartesian3.fromDegrees(-70.59917, -33.44995, 860);
    let nltch = {
        location: nltchLocation,
        // finalView: {
        //     destination : new Cesium.Cartesian3.fromDegrees(152.97768, -27.0956319, 860),
        //     orientation : {
        //         heading : Cesium.Math.toRadians(10),
        //         pitch : Cesium.Math.toRadians(-25),
        //         roll : 0.0
        //     }
        // },
        initialView: {
            destination: nltchLocation,
            duration: flightTime,
            pitchAdjustHeight: pitchAdjustHeight            
        }    
    };
    //nlta.initialView.complete = helpers.animove.bind(null, nlta.finalView);

    return nltch;
}