/** NLT Australia views */
module.exports = (viewer, helpers) => { 
    const Cesium = require("cesium/Cesium");  

    let flightTime = 10;
    let pitchAdjustHeight = 5000000;

    let nltaLocation = new Cesium.Cartesian3.fromDegrees(152.98259, -27.08552, 860);
    let nlta = {
        location: nltaLocation,
        finalView: {
            destination : new Cesium.Cartesian3.fromDegrees(152.97768, -27.0996319, 860),
            orientation : {
                heading : Cesium.Math.toRadians(10),
                pitch : Cesium.Math.toRadians(-25),
                roll : 0.0
            }
        },
        initialView: {
            destination: nltaLocation,
            duration: flightTime,
            pitchAdjustHeight: pitchAdjustHeight            
        }    
    };
    nlta.initialView.complete = helpers.animove.bind(null, nlta.finalView);

    return nlta;
}