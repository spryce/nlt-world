// Mines

// Ensham (148.51442, -23.37075) - This might be the wrong coords. Check later...
// Carborough Downs (148.24762, -21.99237)
// Kestrel South (148.29993, -23.26198)
// Dugald River (140.15533, -20.25072)
// Grasstree (148.57960, -22.98823)


const Cesium = require("cesium/Cesium");
let flightTime = 10;
let pitchAdjustHeight = 5000000;

// Ensham 
const enshamLocation = new Cesium.Cartesian3.fromDegrees(148.51442, -23.37075, 2000);
// Carborough Downs
const carbLocation = new Cesium.Cartesian3.fromDegrees(148.24762, -21.99237, 2000);
// Grasstree 
const grassLocation = new Cesium.Cartesian3.fromDegrees(148.57960, -22.98823, 2000);
// Dugald River 
const dugaldLocation = new Cesium.Cartesian3.fromDegrees(140.15533, -20.25072, 2000);
// Kestrel South 
const kestrelLocation = new Cesium.Cartesian3.fromDegrees(148.29993, -23.26198, 2000);

module.exports = {
    carb: {
        location: carbLocation,
        initialView: {
            destination: carbLocation,
            duration: flightTime,
            pitchAdjustHeight: pitchAdjustHeight
        }
    },
    dugald: {
        location: dugaldLocation,        
        initialView: {
            destination: dugaldLocation,
            duration: flightTime,
            pitchAdjustHeight: pitchAdjustHeight
        }
    },
    ensham: {
        location: enshamLocation,
        initialView: {
            destination: enshamLocation,
            duration: flightTime,
            pitchAdjustHeight: pitchAdjustHeight
        }
    },
    grass: {
        location: grassLocation,
        initialView: {
            destination: grassLocation,
            duration: flightTime,
            pitchAdjustHeight: pitchAdjustHeight
        }
    },
    kestrel: {
        location: kestrelLocation,
        initialView: {
            destination: kestrelLocation,
            duration: flightTime,
            pitchAdjustHeight: pitchAdjustHeight
        }
    }
}
