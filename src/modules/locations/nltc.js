/** NLT Canada views */
module.exports = (viewer, helpers) => { 
    const Cesium = require("cesium/Cesium");

    let flightTime = 10;
    let pitchAdjustHeight = 5000000;

    let nltcLocation = new Cesium.Cartesian3.fromDegrees(-79.36015, 43.70358, 860);
    let nltc = {
        location: nltcLocation,
        finalView: {
            destination : new Cesium.Cartesian3.fromDegrees(-79.36115, 43.69808, 300),
            orientation : {
                heading : Cesium.Math.toRadians(358.0),
                pitch : Cesium.Math.toRadians(-25),
                roll : 0.0
            }
        },
        initialView: {
            destination: nltcLocation,
            duration: flightTime,
            pitchAdjustHeight: pitchAdjustHeight            
        }    
    };
    nltc.initialView.complete = helpers.animove.bind(null, nltc.finalView);

    return nltc;
}