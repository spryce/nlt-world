"use strict"

module.exports  = (viewer, helpers) => { 
    const nlta  = require("./locations/nlta")(viewer, helpers);
    const nltc  = require("./locations/nltc")(viewer, helpers);
    const nltch = require("./locations/nltch")(viewer, helpers);
    const customer = require("./locations/customer");

    return {
        nlta:   nlta,
        nltc:   nltc,
        nltch:  nltch,
        customer: customer
    }
};
