"use strict"

module.exports = (viewer, handler) => {
    const Cesium = require("cesium/Cesium");        
    const viewTimeout = 500;

    const globeSpin = () => {
        viewer.scene.camera.rotateRight(.0025);
    }

    const startSpin = () => {
        viewer.clock.onTick.addEventListener(globeSpin);
    }

    const stopSpin = () => {
        viewer.clock.onTick.removeEventListener(globeSpin);
    }

    return {
        animove: (view) => { 
            setTimeout(function() {            
                viewer.camera.flyTo(view);
                // setTimeout(function() {            
                //     startSpin(); // Set a timeout that refreshes when input is recieved. Otherwise add events and restart spin
                // }, spinTimeout);
            }, viewTimeout);
        },
        flyToYourLocation: () => {            
            let fly = (position) => {
                viewer.camera.flyTo({
                    destination : Cesium.Cartesian3.fromDegrees(position.coords.longitude, position.coords.latitude, 1000.0)
                });
            }
        
            // Ask browser for location, and fly there.
            navigator.geolocation.getCurrentPosition(fly);
        },
        startSpin: startSpin,
        stopSpin: stopSpin,
        setSpinEvents: () => {
            handler.setInputAction(stopSpin, Cesium.ScreenSpaceEventType.LEFT_DOWN);
            handler.setInputAction(startSpin, Cesium.ScreenSpaceEventType.LEFT_UP);
            handler.setInputAction(stopSpin, Cesium.ScreenSpaceEventType.RIGHT_DOWN);
            handler.setInputAction(startSpin, Cesium.ScreenSpaceEventType.RIGHT_UP);
            handler.setInputAction(stopSpin, Cesium.ScreenSpaceEventType.MIDDLE_DOWN);
            handler.setInputAction(startSpin, Cesium.ScreenSpaceEventType.MIDDLE_UP);
        },
        //removeSpinEvents: removeSpinEventHandlers.bind(null, handler),
        removeSpinEvents: () => {
            handler.removeInputAction(Cesium.ScreenSpaceEventType.LEFT_DOWN);
            handler.removeInputAction(Cesium.ScreenSpaceEventType.LEFT_UP);
            handler.removeInputAction(Cesium.ScreenSpaceEventType.RIGHT_DOWN);
            handler.removeInputAction(Cesium.ScreenSpaceEventType.RIGHT_UP);
            handler.removeInputAction(Cesium.ScreenSpaceEventType.MIDDLE_DOWN);
            handler.removeInputAction(Cesium.ScreenSpaceEventType.MIDDLE_UP);
        }        
    };
}

const leftDown = (movement) => {
    stopSpin();
}