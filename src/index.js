"use strict";  

let Cesium = require('cesium/Cesium');
require('cesium/Widgets/widgets.css');
require('./css/main.css');
const viewer = new Cesium.Viewer('cesiumContainer', {
    scene3DOnly: true,
    selectionIndicator: false,
    baseLayerPicker: false
});

const handler = viewer.screenSpaceEventHandler;
const cameraHelpers = require("./modules/camera-helpers")(viewer, handler);
const cameraViews   = require("./modules/camera-views")(viewer, cameraHelpers);
const billboards    = require("./modules/billboards")(viewer, cameraViews);   

viewer.scene.globe.enableLighting = true;    

let initialView = { 
    destination: new Cesium.Cartesian3.fromDegrees(-71.21214, 8.26776, 30000000),       
    orientation : {
        heading : Cesium.Math.toRadians(-10.0),
    }        
};
viewer.scene.camera.setView(initialView);
cameraHelpers.startSpin();      // spin the globe
cameraHelpers.setSpinEvents();  // mouse events to stop / start spin


// The location picker thang
let nltaElement     = document.getElementById('nlta');
let nltcElement     = document.getElementById('nltc');
let nltChileElement = document.getElementById('nltchile');
let yourElement     = document.getElementById('your');
let worldElement    = document.getElementById('world');
let carbElement     = document.getElementById('carb');
let dugaldElement   = document.getElementById('dugald');
let enshamElement   = document.getElementById('ensham');
let grassElement    = document.getElementById('grass');
let kestrelElement  = document.getElementById('kestrel');

function setViewMode() {
    cameraHelpers.stopSpin();
    cameraHelpers.removeSpinEvents();
    // offices
    if (nltaElement.checked) {            
        viewer.scene.camera.flyTo(cameraViews.nlta.initialView);
        return;
    } 
    if (nltcElement.checked) {            
        viewer.scene.camera.flyTo(cameraViews.nltc.initialView);
        return;
    }
    if (nltChileElement.checked) {
        viewer.scene.camera.flyTo(cameraViews.nltch.initialView);
        return;
    }    
    // customers
    if (carbElement.checked) {
        viewer.scene.camera.flyTo(cameraViews.customer.carb.initialView);
        return;
    }
    if (dugaldElement.checked) {
        viewer.scene.camera.flyTo(cameraViews.customer.dugald.initialView);
        return;
    }
    if (enshamElement.checked) {
        viewer.scene.camera.flyTo(cameraViews.customer.ensham.initialView);
        return;
    }    
    if (grassElement.checked) {
        viewer.scene.camera.flyTo(cameraViews.customer.grass.initialView);
        return;
    }
    if (kestrelElement.checked) {
        viewer.scene.camera.flyTo(cameraViews.customer.kestrel.initialView);
        return;
    }
    if (yourElement.checked) {
        cameraHelpers.flyToYourLocation();
        return;
    }
    if (worldElement.checked) {            
        viewer.scene.camera.flyHome(10);
        cameraHelpers.setSpinEvents();
        cameraHelpers.startSpin();
        return;
    }
}

nltaElement.addEventListener('change',      setViewMode);
nltcElement.addEventListener('change',      setViewMode);
nltChileElement.addEventListener('change',  setViewMode);
carbElement.addEventListener('change',      setViewMode);
dugaldElement.addEventListener('change',    setViewMode);
enshamElement.addEventListener('change',    setViewMode);
grassElement.addEventListener('change',     setViewMode);
kestrelElement.addEventListener('change',   setViewMode);
yourElement.addEventListener('change',      setViewMode);
worldElement.addEventListener('change',     setViewMode);

const spinGlobe = () => {   
    if (spin.checked) {
        cameraHelpers.startSpin();
        cameraHelpers.setSpinEvents();
        return;
    }
    cameraHelpers.stopSpin();
    cameraHelpers.removeSpinEvents();
};

let spinElement  = document.getElementById('spin');
spinElement.addEventListener('change',     spinGlobe);